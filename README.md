## Script to make a custom folder Structure using Rust :crab:

1. Set root base folder uisng CLI argument.
2. Set the folder's structure.

The structure would look like:

- Root folder (decimalMonth-LiteralMonth_DayOfTheMonth) => (05-MAY_15)
  - Historia_Clinica
    - HC_jpeg
  - Laboratorios
    - Resultados
  - Ordenes_Medicas
    - MICO_TACRO_jpeg
    - AUTORIZACIONES
