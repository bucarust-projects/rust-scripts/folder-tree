use std::{fs, path::{Path, PathBuf}, io::{self, Write}, env};

fn main() -> std::io::Result<()> {
    let cli_args: Vec<String> = env::args().collect();
    
    let base_folder = match Path::new(&cli_args[1]).canonicalize() {
        Ok(absolute_path) => absolute_path,
        Err(e) => {
            println!("Error {:#?}", e);
            PathBuf::new()
        }
    };

    let mut folder_date = String::new();

    print!("Add your appointment date folder:\n");
    print!("(decimalMonth-LiteralMonth_DayOfTheMonth) => (05-MAY_15): {}", &folder_date);

    match io::stdout().flush() {
        Ok(_) => {},
        Err(e) => {
            panic!("Input Error, {:#?}", e)
        }
    };

    match io::stdin().read_line(&mut folder_date) {
        Ok(_) => {}
        Err(_) => {
            panic!("Panic out")
        }
    };
    
    let trim_folder_date = folder_date.trim();
    let inner_folders: [&str; 3] = ["Historia_Clinica", "Laboratorios", "Ordenes_Medicas"];
    let ordenes_medicas_subfolders: [&str; 2] = ["MICO_TACRO_jpeg", "AUTORIZACIONES"];
    
    let _: () = inner_folders.iter().map(|&folder| {
        let mut nested_folder = base_folder.join(trim_folder_date).join(folder);
        match folder {
            "Historia_Clinica" => {
                nested_folder = nested_folder.join("HC_jpeg");
            }
            "Laboratorios" => {
                nested_folder = nested_folder.join("Resultados");
            }
            _ => {
                let _: () = ordenes_medicas_subfolders.iter().map(|&subfolder| {
                    let mut aditional_folder = base_folder.join(trim_folder_date).join(folder); 
                    match subfolder {
                        subfolder => {
                            aditional_folder = aditional_folder.join(subfolder);
                        }
                    }
                    println!("{:?}", &aditional_folder);
                    match fs::create_dir_all(&aditional_folder) {
                        Ok(()) => {},
                        Err(_) => {
                            panic!("No folder has been created");
                        }
                    };
                }).collect::<()>();
            }
        };
        println!("{:?}", &nested_folder);
        match fs::create_dir_all(&nested_folder) {
            Ok(()) => {}
            Err(_) => {
                panic!("No folder has been created");
            }
        };
    }).collect::<()>();
    print!("Your folder date created is {}", folder_date);
    print!("location: {:#?}", &base_folder);


    Ok(())

}
